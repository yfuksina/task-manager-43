package ru.tsc.fuksina.tm.exception.field;

public final class PasswordIncorrectException extends AbstractFieldException {

    public PasswordIncorrectException() {
        super("Error! Password is incorrect...");
    }

}
