package ru.tsc.fuksina.tm.exception.system;

import ru.tsc.fuksina.tm.exception.AbstractException;

public class AuthenticationException extends AbstractException {

    public AuthenticationException() {
        super("Error! Login or password is incorrect...");
    }

}
