package ru.tsc.fuksina.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.fuksina.tm.api.repository.model.IUserOwnedRepository;
import ru.tsc.fuksina.tm.model.AbstractUserOwnedModel;

import java.util.Date;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    @NotNull
    M create(@Nullable String userId, @Nullable String name);

    @NotNull
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateStart,
            @Nullable Date dateEnd
    );

    @NotNull
    M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}
